#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
        string      str;
        string      subStr;
        vector<int> kmpVector;
    
        void display()
        {
            int len = (int)kmpVector.size();
            for(int i = 0 ; i < len ; i++)
            {
                cout<<kmpVector[i]<<" ";
            }
            cout<<endl;
        }
    
        void prepareKMPChart()
        {
            int len = (int)subStr.length();
            int j   = 0;
            kmpVector.push_back(0);
            for(int i = 1 ; i < len ; i++)
            {
                if(subStr[i] == subStr[j])
                {
                    kmpVector.push_back(j+1);
                    j++;
                }
                else
                {
                    if(!j)
                    {
                        kmpVector.push_back(0);
                        if(str[i] == str[j])
                        {
                            j++;
                        }
                    }
                    else
                    {
                        j = kmpVector[j-1];
                        i--;
                    }
                }
            }
        }
    
    public:
        Engine(string s , string sS)
        {
            str    = s;
            subStr = sS;
            prepareKMPChart();
        }
    
        int findLongestPrefixSuffixArray()
        {
            int maxCount  = 0;
            int count     = 0;
            int subStrLen = (int)subStr.length();
            int strLen    = (int)str.length();
            int j         = 0;
            for(int i = 0 ; i < strLen && j < subStrLen ; i++)
            {
                if(str[i] == subStr[j])
                {
                    count++;
                    maxCount = count > maxCount ? count : maxCount;
                    j++;
                }
                else
                {
                    maxCount = count > maxCount ? count : maxCount;
                    if(j)
                    {
                        i--;
                        j = kmpVector[j-1];
                    }
                    count = j;
                }
            }
            return maxCount;
        }
};

int main(int argc, const char * argv[])
{
//    Engine e = Engine("abxabcabcaby", "abcaby");
    Engine e = Engine("abcxabcdabxabcdabcdabcy", "abcdabcy");
    cout<<e.findLongestPrefixSuffixArray()<<endl;
    return 0;
}
